//
//  main.cpp
//  test
//
//  Created by Jeremy Jurksztowicz on 1/1/2014.
//  Copyright (c) 2014 eclectocrat. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cassert>
#include "libtcod.h"
#include "namegen.h"

using namespace std;

typedef string String;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
namespace ec {
namespace name {

vector<String> nameTypes;

template<typename Iter>
void LoadNamePacks (Iter b, Iter e)
{
    assert(b <= e);
    TCOD_random_t r = TCOD_random_get_instance();
    while(b < e)
    {
        ifstream file(*b);
        String line;
        while(getline(file, line))
        {
            String::size_type n = line.find("name \"");
            if(n != String::npos)
            {
                String::size_type endn = line.find("\" {");
                if(endn != String::npos)
                {
                    String nameType = line.substr(n+6, endn-6);
                    
                    // DEBUG
                    // cout << "Found name type '" << nameType << "'" << endl;
                    nameTypes.push_back(nameType);
                }
            }
        }
        
        if(nameTypes.empty())
            cerr << "Error! Couldn't find any names in name pack " << *b << endl;
        else
            TCOD_namegen_parse(b->c_str(), r);
        b++;
    }
};

String GenerateName (String const& nameType)
{
    char * newName = TCOD_namegen_generate(
        const_cast<char*>(nameType.c_str()),
        false);
    return String(newName);
}

} // END namespace name
} // END namespace ec
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
int main(int argc, const char * argv[])
{
    String root = "/Users/jeremy.jurksztowicz/Desktop/libtcod_custom/namegen/";
    vector<String> packs = {
        root + "jice_celtic.cfg",
        root + "jice_fantasy.cfg",
        root + "jice_mesopotamian.cfg",
        root + "jice_norse.cfg",
        root + "jice_region.cfg",
        root + "jice_town.cfg",
        root + "mingos_demon.cfg",
        root + "mingos_dwarf.cfg",
        root + "mingos_norse.cfg",
        root + "mingos_standard.cfg",
        root + "mingos_town.cfg"
    };

    ec::name::LoadNamePacks(packs.begin(), packs.end());
    
    for(int i=0; i<100; ++i)
    {
        const int r = TCOD_random_get_int(TCOD_random_get_instance(), 0, ec::name::nameTypes.size()-1);
    
        cout<< ec::name::nameTypes[r]
            << " => "
            << ec::name::GenerateName(ec::name::nameTypes[r])
            << endl;
    }
    return 0;
}
